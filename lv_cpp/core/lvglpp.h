/*
 * lvglpp.h
 *
 *  Created on: Jun 24, 2021
 *      Author: fstuffdev
 */

#ifndef LVGLPP_H_
#define LVGLPP_H_

#include "../../lvgl/lvgl.h"

#include <memory>
#include <functional>
#include <vector>

namespace lvglpp
{

	/* Template for custom deleter function */
	template <auto DeleterFunction>
	using CustomDeleter = std::integral_constant<decltype(DeleterFunction), DeleterFunction>;

	/* Used for create a smart_pointer with custom deleter */
	template <typename ManagedType, auto Functor>
	using LvPointer = std::unique_ptr<ManagedType, CustomDeleter<Functor>>;

	/* Used for create a smart_pointer with default deleter for cpp object */
	template <typename ManagedType>
	using LvPointerUnique = LvPointer<ManagedType, lv_mem_free>;

	/* Variadic class contructor */
	template <typename Class, typename... ArgsT>
	LvPointerUnique<Class> Make(ArgsT &&...args)
	{
		return LvPointerUnique<Class>{new (lv_mem_alloc(sizeof(Class))) Class{std::forward<ArgsT>(args)...}};
	};

	template <typename PROTYPE>
	class LvTObj;

	/* lvgl iterface functions */
	/* Initilize the lvglpp library */
	void Init();
	void Handler(unsigned int ms);
	void DefaultPeripheral();

} /* namespace lvglpp */

#endif /* LVGLPP_H_ */
