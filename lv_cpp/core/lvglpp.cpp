/*
 * lvglpp.cpp
 *
 *  Created on: Jul 19, 2021
 *      Author: fstuffdev
 */

#include "lvglpp.h"
#include "LvIndev.h"

namespace lvglpp
{

	/* Init lvgl */
	void Init()
	{
		lv_init();
	}

	void DefaultPeripheral()
	{
	}

	void Handler(unsigned int ms)
	{
#if !LV_TICK_CUSTOM
		lv_tick_inc(ms);
#else
		(void)ms;
#endif
		lv_timer_handler();
	}

} // namespace lvglpp
