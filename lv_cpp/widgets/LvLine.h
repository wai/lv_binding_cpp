
#ifndef LVLINE_H_
#define LVLINE_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTLine : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setPoints(const lv_point_t points[], uint16_t point_num) { return lv_line_set_points(raw(), points, point_num), _self(); }
		inline TSelf &setYInvert(bool en) { return lv_line_set_y_invert(raw(), en), _self(); }
		inline bool getYInvert() const { return lv_line_get_y_invert(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_line_t>
	{
		using TWrapped = LvTLine<lv_line_t>;
		static constexpr auto &obj_class = lv_line_class;
		static constexpr auto create = lv_line_create;
	};

	using LvLine = LvTLine<lv_line_t &&>;
	using LvLinePtr = LvRawPtr<lv_line_t>;

} /* namespace lvglpp */

#endif /* LVLINE_H_ */
