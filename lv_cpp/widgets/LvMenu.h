
#ifndef LVMENU_H_
#define LVMENU_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTMenu : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline LvObjPtr pageCreate(char *title) { return (LvObjPtr)lv_menu_page_create(raw(), title); }
		inline LvObjPtr contCreate() { return (LvObjPtr)lv_menu_cont_create(raw()); }
		inline LvObjPtr sectionCreate() { return (LvObjPtr)lv_menu_section_create(raw()); }
		inline LvObjPtr separatorCreate() { return (LvObjPtr)lv_menu_separator_create(raw()); }
		inline TSelf &setPage(lv_obj_t *page) { return lv_menu_set_page(raw(), page), _self(); }
		inline TSelf &setSidebarPage(lv_obj_t *page) { return lv_menu_set_sidebar_page(raw(), page), _self(); }
		inline TSelf &setModeHeader(lv_menu_mode_header_t mode_header) { return lv_menu_set_mode_header(raw(), mode_header), _self(); }
		inline TSelf &setModeRootBackBtn(lv_menu_mode_root_back_btn_t mode_root_back_btn) { return lv_menu_set_mode_root_back_btn(raw(), mode_root_back_btn), _self(); }
		inline TSelf &setLoadPageEvent(lv_obj_t *obj, lv_obj_t *page) { return lv_menu_set_load_page_event(raw(), obj, page), _self(); }
		inline LvObjPtr getCurMainPage() { return (LvObjPtr)lv_menu_get_cur_main_page(raw()); }
		inline LvObjPtr getCurSidebarPage() { return (LvObjPtr)lv_menu_get_cur_sidebar_page(raw()); }
		inline LvObjPtr getMainHeader() { return (LvObjPtr)lv_menu_get_main_header(raw()); }
		inline LvObjPtr getMainHeaderBackBtn() { return (LvObjPtr)lv_menu_get_main_header_back_btn(raw()); }
		inline LvObjPtr getSidebarHeader() { return (LvObjPtr)lv_menu_get_sidebar_header(raw()); }
		inline LvObjPtr getSidebarHeaderBackBtn() { return (LvObjPtr)lv_menu_get_sidebar_header_back_btn(raw()); }
		inline bool backBtnIsRoot(lv_obj_t *obj) { return lv_menu_back_btn_is_root(raw(), obj); }
		inline TSelf &clearHistory() { return lv_menu_clear_history(raw()), _self(); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_menu_t>
	{
		using TWrapped = LvTMenu<lv_menu_t>;
		static constexpr auto &obj_class = lv_menu_class;
		static constexpr auto create = lv_menu_create;
	};

	using LvMenu = LvTMenu<lv_menu_t &&>;
	using LvMenuPtr = LvRawPtr<lv_menu_t>;

} /* namespace lvglpp */

#endif /* LVMENU_H_ */
