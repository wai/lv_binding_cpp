
#ifndef LVANIMIMG_H_
#define LVANIMIMG_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTAnimimg : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setSrc(lv_img_dsc_t *dsc[], uint8_t num) { return lv_animimg_set_src(raw(), dsc, num), _self(); }
		inline TSelf &start() { return lv_animimg_start(raw()), _self(); }
		inline TSelf &setDuration(uint32_t duration) { return lv_animimg_set_duration(raw(), duration), _self(); }
		inline TSelf &setRepeatCount(uint16_t count) { return lv_animimg_set_repeat_count(raw(), count), _self(); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_animimg_t>
	{
		using TWrapped = LvTAnimimg<lv_animimg_t>;
		static constexpr auto &obj_class = lv_animimg_class;
		static constexpr auto create = lv_animimg_create;
	};

	using LvAnimimg = LvTAnimimg<lv_animimg_t &&>;
	using LvAnimimgPtr = LvRawPtr<lv_animimg_t>;

} /* namespace lvglpp */

#endif /* LVANIMIMG_H_ */
