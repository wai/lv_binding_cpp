
#ifndef LVSPINNER_H_
#define LVSPINNER_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTSpinner : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		//+gen
	};

	struct lv_spinner_t{lv_obj_t obj;};

	template <>
	struct LvProtypeTraits<lv_spinner_t>
	{
		using TWrapped = LvTSpinner<lv_spinner_t>;
		static constexpr auto &obj_class = lv_spinner_class;
		static constexpr auto create = lv_spinner_create;
	};

	using LvSpinner = LvTSpinner<lv_spinner_t &&>;
	using LvSpinnerPtr = LvRawPtr<lv_spinner_t>;

} /* namespace lvglpp */

#endif /* LVSPINNER_H_ */
