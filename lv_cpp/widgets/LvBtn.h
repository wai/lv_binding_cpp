
#ifndef LVBTN_H_
#define LVBTN_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTBtn : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_btn_t>
	{
		using TWrapped = LvTBtn<lv_btn_t>;
		static constexpr auto &obj_class = lv_btn_class;
		static constexpr auto create = lv_btn_create;
	};

	using LvBtn = LvTBtn<lv_btn_t &&>;
	using LvBtnPtr = LvRawPtr<lv_btn_t>;

} /* namespace lvglpp */

#endif /* LVBTN_H_ */
