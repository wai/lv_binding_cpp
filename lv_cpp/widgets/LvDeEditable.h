#ifndef LVDEEDITABLE_H_
#define LVDEEDITABLE_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	struct lv_deEditable_t : public PROTYPE
	{
	};

	template <typename PROTYPE>
	struct LvTDeEditable;

	template <typename PROTYPE>
	struct LvTDeEditable<lv_deEditable_t<PROTYPE>> : public _lv::WrappedTraits<typename LvProtypeTraits<PROTYPE>::TWrapped>::template apply<lv_deEditable_t<PROTYPE>>
	{
		using _lv::WrappedTraits<typename LvProtypeTraits<PROTYPE>::TWrapped>::template apply<lv_deEditable_t<PROTYPE>>::apply;
	};

	template <typename PROTYPE>
	struct LvTDeEditable<lv_deEditable_t<PROTYPE> &&> : public _lv::WrappedTraits<typename LvProtypeTraits<PROTYPE>::TWrapped>::template apply<lv_deEditable_t<PROTYPE> &&>
	{
		using _lv::WrappedTraits<typename LvProtypeTraits<PROTYPE>::TWrapped>::template apply<lv_deEditable_t<PROTYPE> &&>::apply;
	};

	template <typename PROTYPE>
	struct LvProtypeTraits<lv_deEditable_t<PROTYPE>>
	{
		using TWrapped = LvTDeEditable<lv_deEditable_t<PROTYPE>>;
		static constexpr lv_obj_class_t obj_class = {.base_class = &LvProtypeTraits<PROTYPE>::obj_class, .editable = LV_OBJ_CLASS_EDITABLE_FALSE};
		static constexpr auto create(lv_obj_t *parent)
		{
			lv_obj_t *obj = LvProtypeTraits<PROTYPE>::create(parent);
			obj->class_p = &obj_class;
			return obj;
		}
	};

	template <typename PROTYPE>
	using LvDeEditable = LvTDeEditable<lv_deEditable_t<PROTYPE> &&>;

	template <typename PROTYPE>
	using LvDeEditablePtr = LvRawPtr<lv_deEditable_t<PROTYPE>>;

} // namespace lvglpp

#endif
