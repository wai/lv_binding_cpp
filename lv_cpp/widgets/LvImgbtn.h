
#ifndef LVIMGBTN_H_
#define LVIMGBTN_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTImgbtn : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setSrc(lv_imgbtn_state_t state, const void *src_left, const void *src_mid, const void *src_right) { return lv_imgbtn_set_src(raw(), state, src_left, src_mid, src_right), _self(); }
		inline TSelf &setState(lv_imgbtn_state_t state) { return lv_imgbtn_set_state(raw(), state), _self(); }
		inline const void *getSrcLeft(lv_imgbtn_state_t state) { return lv_imgbtn_get_src_left(raw(), state); }
		inline const void *getSrcMiddle(lv_imgbtn_state_t state) { return lv_imgbtn_get_src_middle(raw(), state); }
		inline const void *getSrcRight(lv_imgbtn_state_t state) { return lv_imgbtn_get_src_right(raw(), state); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_imgbtn_t>
	{
		using TWrapped = LvTImgbtn<lv_imgbtn_t>;
		static constexpr auto &obj_class = lv_imgbtn_class;
		static constexpr auto create = lv_imgbtn_create;
	};

	using LvImgbtn = LvTImgbtn<lv_imgbtn_t &&>;
	using LvImgbtnPtr = LvRawPtr<lv_imgbtn_t>;

} /* namespace lvglpp */

#endif /* LVIMGBTN_H_ */
