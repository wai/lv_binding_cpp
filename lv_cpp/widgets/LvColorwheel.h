
#ifndef LVCOLORWHEEL_H_
#define LVCOLORWHEEL_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTColorwheel : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline bool setHsv(lv_color_hsv_t hsv) { return lv_colorwheel_set_hsv(raw(), hsv); }
		inline bool setRgb(lv_color_t color) { return lv_colorwheel_set_rgb(raw(), color); }
		inline TSelf &setMode(lv_colorwheel_mode_t mode) { return lv_colorwheel_set_mode(raw(), mode), _self(); }
		inline TSelf &setModeFixed(bool fixed) { return lv_colorwheel_set_mode_fixed(raw(), fixed), _self(); }
		inline lv_color_hsv_t getHsv() { return lv_colorwheel_get_hsv(raw()); }
		inline lv_color_t getRgb() { return lv_colorwheel_get_rgb(raw()); }
		inline lv_colorwheel_mode_t getColorMode() { return lv_colorwheel_get_color_mode(raw()); }
		inline bool getColorModeFixed() { return lv_colorwheel_get_color_mode_fixed(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_colorwheel_t>
	{
		using TWrapped = LvTColorwheel<lv_colorwheel_t>;
		static constexpr auto &obj_class = lv_colorwheel_class;
		static constexpr auto create = lv_colorwheel_create;
	};

	using LvColorwheel = LvTColorwheel<lv_colorwheel_t &&>;
	using LvColorwheelPtr = LvRawPtr<lv_colorwheel_t>;

} /* namespace lvglpp */

#endif /* LVCOLORWHEEL_H_ */
