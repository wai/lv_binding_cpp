
#ifndef LVMETER_H_
#define LVMETER_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTMeter : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline lv_meter_scale_t *addScale() { return lv_meter_add_scale(raw()); }
		inline TSelf &setScaleTicks(lv_meter_scale_t *scale, uint16_t cnt, uint16_t width, uint16_t len, lv_color_t color) { return lv_meter_set_scale_ticks(raw(), scale, cnt, width, len, color), _self(); }
		inline TSelf &setScaleMajorTicks(lv_meter_scale_t *scale, uint16_t nth, uint16_t width, uint16_t len, lv_color_t color, int16_t label_gap) { return lv_meter_set_scale_major_ticks(raw(), scale, nth, width, len, color, label_gap), _self(); }
		inline TSelf &setScaleRange(lv_meter_scale_t *scale, int32_t min, int32_t max, uint32_t angle_range, uint32_t rotation) { return lv_meter_set_scale_range(raw(), scale, min, max, angle_range, rotation), _self(); }
		inline lv_meter_indicator_t *addNeedleLine(lv_meter_scale_t *scale, uint16_t width, lv_color_t color, int16_t r_mod) { return lv_meter_add_needle_line(raw(), scale, width, color, r_mod); }
		inline lv_meter_indicator_t *addNeedleImg(lv_meter_scale_t *scale, const void *src, lv_coord_t pivot_x, lv_coord_t pivot_y) { return lv_meter_add_needle_img(raw(), scale, src, pivot_x, pivot_y); }
		inline lv_meter_indicator_t *addArc(lv_meter_scale_t *scale, uint16_t width, lv_color_t color, int16_t r_mod) { return lv_meter_add_arc(raw(), scale, width, color, r_mod); }
		inline lv_meter_indicator_t *addScaleLines(lv_meter_scale_t *scale, lv_color_t color_start, lv_color_t color_end, bool local, int16_t width_mod) { return lv_meter_add_scale_lines(raw(), scale, color_start, color_end, local, width_mod); }
		inline TSelf &setIndicatorValue(lv_meter_indicator_t *indic, int32_t value) { return lv_meter_set_indicator_value(raw(), indic, value), _self(); }
		inline TSelf &setIndicatorStartValue(lv_meter_indicator_t *indic, int32_t value) { return lv_meter_set_indicator_start_value(raw(), indic, value), _self(); }
		inline TSelf &setIndicatorEndValue(lv_meter_indicator_t *indic, int32_t value) { return lv_meter_set_indicator_end_value(raw(), indic, value), _self(); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_meter_t>
	{
		using TWrapped = LvTMeter<lv_meter_t>;
		static constexpr auto &obj_class = lv_meter_class;
		static constexpr auto create = lv_meter_create;
	};

	using LvMeter = LvTMeter<lv_meter_t &&>;
	using LvMeterPtr = LvRawPtr<lv_meter_t>;

} /* namespace lvglpp */

#endif /* LVMETER_H_ */
