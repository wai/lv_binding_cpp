
#ifndef LVTABLE_H_
#define LVTABLE_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTTable : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setCellValue(uint16_t row, uint16_t col, const char *txt) { return lv_table_set_cell_value(raw(), row, col, txt), _self(); }
		inline TSelf &setCellValueFmt(uint16_t row, uint16_t col, const char *fmt) { return lv_table_set_cell_value_fmt(raw(), row, col, fmt), _self(); }
		inline TSelf &setRowCnt(uint16_t row_cnt) { return lv_table_set_row_cnt(raw(), row_cnt), _self(); }
		inline TSelf &setColCnt(uint16_t col_cnt) { return lv_table_set_col_cnt(raw(), col_cnt), _self(); }
		inline TSelf &setColWidth(uint16_t col_id, lv_coord_t w) { return lv_table_set_col_width(raw(), col_id, w), _self(); }
		inline TSelf &addCellCtrl(uint16_t row, uint16_t col, lv_table_cell_ctrl_t ctrl) { return lv_table_add_cell_ctrl(raw(), row, col, ctrl), _self(); }
		inline TSelf &clearCellCtrl(uint16_t row, uint16_t col, lv_table_cell_ctrl_t ctrl) { return lv_table_clear_cell_ctrl(raw(), row, col, ctrl), _self(); }
		inline const char *getCellValue(uint16_t row, uint16_t col) { return lv_table_get_cell_value(raw(), row, col); }
		inline uint16_t getRowCnt() { return lv_table_get_row_cnt(raw()); }
		inline uint16_t getColCnt() { return lv_table_get_col_cnt(raw()); }
		inline lv_coord_t getColWidth(uint16_t col) { return lv_table_get_col_width(raw(), col); }
		inline bool hasCellCtrl(uint16_t row, uint16_t col, lv_table_cell_ctrl_t ctrl) { return lv_table_has_cell_ctrl(raw(), row, col, ctrl); }
		inline TSelf &getSelectedCell(uint16_t *row, uint16_t *col) { return lv_table_get_selected_cell(raw(), row, col), _self(); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_table_t>
	{
		using TWrapped = LvTTable<lv_table_t>;
		static constexpr auto &obj_class = lv_table_class;
		static constexpr auto create = lv_table_create;
	};

	using LvTable = LvTTable<lv_table_t &&>;
	using LvTablePtr = LvRawPtr<lv_table_t>;

} /* namespace lvglpp */

#endif /* LVTABLE_H_ */
