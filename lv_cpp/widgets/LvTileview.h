
#ifndef LVTILEVIEW_H_
#define LVTILEVIEW_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTTileview : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline LvObjPtr addTile(uint8_t col_id, uint8_t row_id, lv_dir_t dir) { return (LvObjPtr)lv_tileview_add_tile(raw(), col_id, row_id, dir); }
		inline LvObjPtr getTileAct() { return (LvObjPtr)lv_tileview_get_tile_act(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_tileview_t>
	{
		using TWrapped = LvTTileview<lv_tileview_t>;
		static constexpr auto &obj_class = lv_tileview_class;
		static constexpr auto create = lv_tileview_create;
	};

	using LvTileview = LvTTileview<lv_tileview_t &&>;
	using LvTileviewPtr = LvRawPtr<lv_tileview_t>;

} /* namespace lvglpp */

#endif /* LVTILEVIEW_H_ */
