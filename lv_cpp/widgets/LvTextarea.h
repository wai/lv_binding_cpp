
#ifndef LVTEXTAREA_H_
#define LVTEXTAREA_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTTextarea : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &addChar(uint32_t c) { return lv_textarea_add_char(raw(), c), _self(); }
		inline TSelf &addText(const char *txt) { return lv_textarea_add_text(raw(), txt), _self(); }
		inline TSelf &delChar() { return lv_textarea_del_char(raw()), _self(); }
		inline TSelf &delCharForward() { return lv_textarea_del_char_forward(raw()), _self(); }
		inline TSelf &setText(const char *txt) { return lv_textarea_set_text(raw(), txt), _self(); }
		inline TSelf &setPlaceholderText(const char *txt) { return lv_textarea_set_placeholder_text(raw(), txt), _self(); }
		inline TSelf &setCursorPos(int32_t pos) { return lv_textarea_set_cursor_pos(raw(), pos), _self(); }
		inline TSelf &setCursorClickPos(bool en) { return lv_textarea_set_cursor_click_pos(raw(), en), _self(); }
		inline TSelf &setPasswordMode(bool en) { return lv_textarea_set_password_mode(raw(), en), _self(); }
		inline TSelf &setOneLine(bool en) { return lv_textarea_set_one_line(raw(), en), _self(); }
		inline TSelf &setAcceptedChars(const char *list) { return lv_textarea_set_accepted_chars(raw(), list), _self(); }
		inline TSelf &setMaxLength(uint32_t num) { return lv_textarea_set_max_length(raw(), num), _self(); }
		inline TSelf &setInsertReplace(const char *txt) { return lv_textarea_set_insert_replace(raw(), txt), _self(); }
		inline TSelf &setTextSelection(bool en) { return lv_textarea_set_text_selection(raw(), en), _self(); }
		inline TSelf &setPasswordShowTime(uint16_t time) { return lv_textarea_set_password_show_time(raw(), time), _self(); }
		inline TSelf &setAlign(lv_text_align_t align) { return lv_textarea_set_align(raw(), align), _self(); }
		inline const char *getText() const { return lv_textarea_get_text(raw()); }
		inline const char *getPlaceholderText() { return lv_textarea_get_placeholder_text(raw()); }
		inline LvObjPtr getLabel() const { return (LvObjPtr)lv_textarea_get_label(raw()); }
		inline uint32_t getCursorPos() const { return lv_textarea_get_cursor_pos(raw()); }
		inline bool getCursorClickPos() { return lv_textarea_get_cursor_click_pos(raw()); }
		inline bool getPasswordMode() const { return lv_textarea_get_password_mode(raw()); }
		inline bool getOneLine() const { return lv_textarea_get_one_line(raw()); }
		inline const char *getAcceptedChars() { return lv_textarea_get_accepted_chars(raw()); }
		inline uint32_t getMaxLength() { return lv_textarea_get_max_length(raw()); }
		inline bool textIsSelected() const { return lv_textarea_text_is_selected(raw()); }
		inline bool getTextSelection() { return lv_textarea_get_text_selection(raw()); }
		inline uint16_t getPasswordShowTime() { return lv_textarea_get_password_show_time(raw()); }
		inline TSelf &clearSelection() { return lv_textarea_clear_selection(raw()), _self(); }
		inline TSelf &cursorRight() { return lv_textarea_cursor_right(raw()), _self(); }
		inline TSelf &cursorLeft() { return lv_textarea_cursor_left(raw()), _self(); }
		inline TSelf &cursorDown() { return lv_textarea_cursor_down(raw()), _self(); }
		inline TSelf &cursorUp() { return lv_textarea_cursor_up(raw()), _self(); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_textarea_t>
	{
		using TWrapped = LvTTextarea<lv_textarea_t>;
		static constexpr auto &obj_class = lv_textarea_class;
		static constexpr auto create = lv_textarea_create;
	};

	using LvTextarea = LvTTextarea<lv_textarea_t &&>;
	using LvTextareaPtr = LvRawPtr<lv_textarea_t>;

} /* namespace lvglpp */

#endif /* LVTEXTAREA_H_ */
