
#ifndef LVSWITCH_H_
#define LVSWITCH_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTSwitch : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_switch_t>
	{
		using TWrapped = LvTSwitch<lv_switch_t>;
		static constexpr auto &obj_class = lv_switch_class;
		static constexpr auto create = lv_switch_create;
	};

	using LvSwitch = LvTSwitch<lv_switch_t &&>;
	using LvSwitchPtr = LvRawPtr<lv_switch_t>;

} /* namespace lvglpp */

#endif /* LVSWITCH_H_ */
