
#ifndef LVROLLER_H_
#define LVROLLER_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTRoller : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setOptions(const char *options, lv_roller_mode_t mode) { return lv_roller_set_options(raw(), options, mode), _self(); }
		inline TSelf &setSelected(uint16_t sel_opt, lv_anim_enable_t anim) { return lv_roller_set_selected(raw(), sel_opt, anim), _self(); }
		inline TSelf &setVisibleRowCount(uint8_t row_cnt) { return lv_roller_set_visible_row_count(raw(), row_cnt), _self(); }
		inline uint16_t getSelected() const { return lv_roller_get_selected(raw()); }
		inline const TSelf &getSelectedStr(char *buf, uint32_t buf_size) const { return lv_roller_get_selected_str(raw(), buf, buf_size), _self(); }
		inline const char *getOptions() const { return lv_roller_get_options(raw()); }
		inline uint16_t getOptionCnt() const { return lv_roller_get_option_cnt(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_roller_t>
	{
		using TWrapped = LvTRoller<lv_roller_t>;
		static constexpr auto &obj_class = lv_roller_class;
		static constexpr auto create = lv_roller_create;
	};

	using LvRoller = LvTRoller<lv_roller_t &&>;
	using LvRollerPtr = LvRawPtr<lv_roller_t>;

} /* namespace lvglpp */

#endif /* LVROLLER_H_ */
