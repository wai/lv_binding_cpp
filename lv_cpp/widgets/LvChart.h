
#ifndef LVCHART_H_
#define LVCHART_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTChart : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setType(lv_chart_type_t type) { return lv_chart_set_type(raw(), type), _self(); }
		inline TSelf &setPointCount(uint16_t cnt) { return lv_chart_set_point_count(raw(), cnt), _self(); }
		inline TSelf &setRange(lv_chart_axis_t axis, lv_coord_t min, lv_coord_t max) { return lv_chart_set_range(raw(), axis, min, max), _self(); }
		inline TSelf &setUpdateMode(lv_chart_update_mode_t update_mode) { return lv_chart_set_update_mode(raw(), update_mode), _self(); }
		inline TSelf &setDivLineCount(uint8_t hdiv, uint8_t vdiv) { return lv_chart_set_div_line_count(raw(), hdiv, vdiv), _self(); }
		inline TSelf &setZoomX(uint16_t zoom_x) { return lv_chart_set_zoom_x(raw(), zoom_x), _self(); }
		inline TSelf &setZoomY(uint16_t zoom_y) { return lv_chart_set_zoom_y(raw(), zoom_y), _self(); }
		inline uint16_t getZoomX() const { return lv_chart_get_zoom_x(raw()); }
		inline uint16_t getZoomY() const { return lv_chart_get_zoom_y(raw()); }
		inline TSelf &setAxisTick(lv_chart_axis_t axis, lv_coord_t major_len, lv_coord_t minor_len, lv_coord_t major_cnt, lv_coord_t minor_cnt, bool label_en, lv_coord_t draw_size) { return lv_chart_set_axis_tick(raw(), axis, major_len, minor_len, major_cnt, minor_cnt, label_en, draw_size), _self(); }
		inline lv_chart_type_t getType() const { return lv_chart_get_type(raw()); }
		inline uint16_t getPointCount() const { return lv_chart_get_point_count(raw()); }
		inline uint16_t getXStartPoint(lv_chart_series_t *ser) const { return lv_chart_get_x_start_point(raw(), ser); }
		inline TSelf &getPointPosById(lv_chart_series_t *ser, uint16_t id, lv_point_t *p_out) { return lv_chart_get_point_pos_by_id(raw(), ser, id, p_out), _self(); }
		inline TSelf &refresh() { return lv_chart_refresh(raw()), _self(); }
		inline lv_chart_series_t *addSeries(lv_color_t color, lv_chart_axis_t axis) { return lv_chart_add_series(raw(), color, axis); }
		inline TSelf &removeSeries(lv_chart_series_t *series) { return lv_chart_remove_series(raw(), series), _self(); }
		inline TSelf &hideSeries(lv_chart_series_t *series, bool hide) { return lv_chart_hide_series(raw(), series, hide), _self(); }
		inline TSelf &setSeriesColor(lv_chart_series_t *series, lv_color_t color) { return lv_chart_set_series_color(raw(), series, color), _self(); }
		inline TSelf &setXStartPoint(lv_chart_series_t *ser, uint16_t id) { return lv_chart_set_x_start_point(raw(), ser, id), _self(); }
		inline lv_chart_series_t *getSeriesNext(const lv_chart_series_t *ser) const { return lv_chart_get_series_next(raw(), ser); }
		inline lv_chart_cursor_t *addCursor(lv_color_t color, lv_dir_t dir) { return lv_chart_add_cursor(raw(), color, dir); }
		inline TSelf &setCursorPos(lv_chart_cursor_t *cursor, lv_point_t *pos) { return lv_chart_set_cursor_pos(raw(), cursor, pos), _self(); }
		inline TSelf &setCursorPoint(lv_chart_cursor_t *cursor, lv_chart_series_t *ser, uint16_t point_id) { return lv_chart_set_cursor_point(raw(), cursor, ser, point_id), _self(); }
		inline lv_point_t getCursorPoint(lv_chart_cursor_t *cursor) { return lv_chart_get_cursor_point(raw(), cursor); }
		inline TSelf &setAllValue(lv_chart_series_t *ser, lv_coord_t value) { return lv_chart_set_all_value(raw(), ser, value), _self(); }
		inline TSelf &setNextValue(lv_chart_series_t *ser, lv_coord_t value) { return lv_chart_set_next_value(raw(), ser, value), _self(); }
		inline TSelf &setNextValue2(lv_chart_series_t *ser, lv_coord_t x_value, lv_coord_t y_value) { return lv_chart_set_next_value2(raw(), ser, x_value, y_value), _self(); }
		inline TSelf &setValueById(lv_chart_series_t *ser, uint16_t id, lv_coord_t value) { return lv_chart_set_value_by_id(raw(), ser, id, value), _self(); }
		inline TSelf &setValueById2(lv_chart_series_t *ser, uint16_t id, lv_coord_t x_value, lv_coord_t y_value) { return lv_chart_set_value_by_id2(raw(), ser, id, x_value, y_value), _self(); }
		inline TSelf &setExtYArray(lv_chart_series_t *ser, lv_coord_t array[]) { return lv_chart_set_ext_y_array(raw(), ser, array), _self(); }
		inline TSelf &setExtXArray(lv_chart_series_t *ser, lv_coord_t array[]) { return lv_chart_set_ext_x_array(raw(), ser, array), _self(); }
		inline lv_coord_t *getYArray(lv_chart_series_t *ser) const { return lv_chart_get_y_array(raw(), ser); }
		inline lv_coord_t *getXArray(lv_chart_series_t *ser) const { return lv_chart_get_x_array(raw(), ser); }
		inline uint32_t getPressedPoint() const { return lv_chart_get_pressed_point(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_chart_t>
	{
		using TWrapped = LvTChart<lv_chart_t>;
		static constexpr auto &obj_class = lv_chart_class;
		static constexpr auto create = lv_chart_create;
	};

	using LvChart = LvTChart<lv_chart_t &&>;
	using LvChartPtr = LvRawPtr<lv_chart_t>;

} /* namespace lvglpp */

#endif /* LVCHART_H_ */
