
#ifndef LVCANVAS_H_
#define LVCANVAS_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTCanvas : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setBuffer(void *buf, lv_coord_t w, lv_coord_t h, lv_img_cf_t cf) { return lv_canvas_set_buffer(raw(), buf, w, h, cf), _self(); }
		inline TSelf &setPxColor(lv_coord_t x, lv_coord_t y, lv_color_t c) { return lv_canvas_set_px_color(raw(), x, y, c), _self(); }
		inline TSelf &setPx(lv_coord_t x, lv_coord_t y, lv_color_t c) { return lv_canvas_set_px(raw(), x, y, c), _self(); }
		inline TSelf &setPxOpa(lv_coord_t x, lv_coord_t y, lv_opa_t opa) { return lv_canvas_set_px_opa(raw(), x, y, opa), _self(); }
		inline TSelf &setPalette(uint8_t id, lv_color_t c) { return lv_canvas_set_palette(raw(), id, c), _self(); }
		inline lv_color_t getPx(lv_coord_t x, lv_coord_t y) { return lv_canvas_get_px(raw(), x, y); }
		inline lv_img_dsc_t *getImg() { return lv_canvas_get_img(raw()); }
		inline TSelf &copyBuf(const void *to_copy, lv_coord_t x, lv_coord_t y, lv_coord_t w, lv_coord_t h) { return lv_canvas_copy_buf(raw(), to_copy, x, y, w, h), _self(); }
		inline TSelf &transform(lv_img_dsc_t *img, int16_t angle, uint16_t zoom, lv_coord_t offset_x, lv_coord_t offset_y, int32_t pivot_x, int32_t pivot_y, bool antialias) { return lv_canvas_transform(raw(), img, angle, zoom, offset_x, offset_y, pivot_x, pivot_y, antialias), _self(); }
		inline TSelf &blurHor(const lv_area_t *area, uint16_t r) { return lv_canvas_blur_hor(raw(), area, r), _self(); }
		inline TSelf &blurVer(const lv_area_t *area, uint16_t r) { return lv_canvas_blur_ver(raw(), area, r), _self(); }
		inline TSelf &fillBg(lv_color_t color, lv_opa_t opa) { return lv_canvas_fill_bg(raw(), color, opa), _self(); }
		inline TSelf &drawRect(lv_coord_t x, lv_coord_t y, lv_coord_t w, lv_coord_t h, const lv_draw_rect_dsc_t *draw_dsc) { return lv_canvas_draw_rect(raw(), x, y, w, h, draw_dsc), _self(); }
		inline TSelf &drawText(lv_coord_t x, lv_coord_t y, lv_coord_t max_w, lv_draw_label_dsc_t *draw_dsc, const char *txt) { return lv_canvas_draw_text(raw(), x, y, max_w, draw_dsc, txt), _self(); }
		inline TSelf &drawImg(lv_coord_t x, lv_coord_t y, const void *src, const lv_draw_img_dsc_t *draw_dsc) { return lv_canvas_draw_img(raw(), x, y, src, draw_dsc), _self(); }
		inline TSelf &drawLine(const lv_point_t points[], uint32_t point_cnt, const lv_draw_line_dsc_t *draw_dsc) { return lv_canvas_draw_line(raw(), points, point_cnt, draw_dsc), _self(); }
		inline TSelf &drawPolygon(const lv_point_t points[], uint32_t point_cnt, const lv_draw_rect_dsc_t *draw_dsc) { return lv_canvas_draw_polygon(raw(), points, point_cnt, draw_dsc), _self(); }
		inline TSelf &drawArc(lv_coord_t x, lv_coord_t y, lv_coord_t r, int32_t start_angle, int32_t end_angle, const lv_draw_arc_dsc_t *draw_dsc) { return lv_canvas_draw_arc(raw(), x, y, r, start_angle, end_angle, draw_dsc), _self(); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_canvas_t>
	{
		using TWrapped = LvTCanvas<lv_canvas_t>;
		static constexpr auto &obj_class = lv_canvas_class;
		static constexpr auto create = lv_canvas_create;
	};

	using LvCanvas = LvTCanvas<lv_canvas_t &&>;
	using LvCanvasPtr = LvRawPtr<lv_canvas_t>;

} /* namespace lvglpp */

#endif /* LVCANVAS_H_ */
