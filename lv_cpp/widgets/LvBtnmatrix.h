
#ifndef LVBTNMATRIX_H_
#define LVBTNMATRIX_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTBtnmatrix : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setMap(const char *map[]) { return lv_btnmatrix_set_map(raw(), map), _self(); }
		inline TSelf &setCtrlMap(const lv_btnmatrix_ctrl_t ctrl_map[]) { return lv_btnmatrix_set_ctrl_map(raw(), ctrl_map), _self(); }
		inline TSelf &setSelectedBtn(uint16_t btn_id) { return lv_btnmatrix_set_selected_btn(raw(), btn_id), _self(); }
		inline TSelf &setBtnCtrl(uint16_t btn_id, lv_btnmatrix_ctrl_t ctrl) { return lv_btnmatrix_set_btn_ctrl(raw(), btn_id, ctrl), _self(); }
		inline TSelf &clearBtnCtrl(uint16_t btn_id, lv_btnmatrix_ctrl_t ctrl) { return lv_btnmatrix_clear_btn_ctrl(raw(), btn_id, ctrl), _self(); }
		inline TSelf &setBtnCtrlAll(lv_btnmatrix_ctrl_t ctrl) { return lv_btnmatrix_set_btn_ctrl_all(raw(), ctrl), _self(); }
		inline TSelf &clearBtnCtrlAll(lv_btnmatrix_ctrl_t ctrl) { return lv_btnmatrix_clear_btn_ctrl_all(raw(), ctrl), _self(); }
		inline TSelf &setBtnWidth(uint16_t btn_id, uint8_t width) { return lv_btnmatrix_set_btn_width(raw(), btn_id, width), _self(); }
		inline TSelf &setOneChecked(bool en) { return lv_btnmatrix_set_one_checked(raw(), en), _self(); }
		inline const char **getMap() const { return lv_btnmatrix_get_map(raw()); }
		inline uint16_t getSelectedBtn() const { return lv_btnmatrix_get_selected_btn(raw()); }
		inline const char *getBtnText(uint16_t btn_id) const { return lv_btnmatrix_get_btn_text(raw(), btn_id); }
		inline bool hasBtnCtrl(uint16_t btn_id, lv_btnmatrix_ctrl_t ctrl) { return lv_btnmatrix_has_btn_ctrl(raw(), btn_id, ctrl); }
		inline bool getOneChecked() const { return lv_btnmatrix_get_one_checked(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_btnmatrix_t>
	{
		using TWrapped = LvTBtnmatrix<lv_btnmatrix_t>;
		static constexpr auto &obj_class = lv_btnmatrix_class;
		static constexpr auto create = lv_btnmatrix_create;
	};

	using LvBtnmatrix = LvTBtnmatrix<lv_btnmatrix_t &&>;
	using LvBtnmatrixPtr = LvRawPtr<lv_btnmatrix_t>;

} /* namespace lvglpp */

#endif /* LVBTNMATRIX_H_ */
