
#ifndef LVCHECKBOX_H_
#define LVCHECKBOX_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTCheckbox : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setText(const char *txt) { return lv_checkbox_set_text(raw(), txt), _self(); }
		inline TSelf &setTextStatic(const char *txt) { return lv_checkbox_set_text_static(raw(), txt), _self(); }
		inline const char *getText() const { return lv_checkbox_get_text(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_checkbox_t>
	{
		using TWrapped = LvTCheckbox<lv_checkbox_t>;
		static constexpr auto &obj_class = lv_checkbox_class;
		static constexpr auto create = lv_checkbox_create;
	};

	using LvCheckbox = LvTCheckbox<lv_checkbox_t &&>;
	using LvCheckboxPtr = LvRawPtr<lv_checkbox_t>;

} /* namespace lvglpp */

#endif /* LVCHECKBOX_H_ */
