
#ifndef LVMSGBOX_H_
#define LVMSGBOX_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTMsgbox : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline LvObjPtr getTitle() { return (LvObjPtr)lv_msgbox_get_title(raw()); }
		inline LvObjPtr getCloseBtn() { return (LvObjPtr)lv_msgbox_get_close_btn(raw()); }
		inline LvObjPtr getText() { return (LvObjPtr)lv_msgbox_get_text(raw()); }
		inline LvObjPtr getContent() { return (LvObjPtr)lv_msgbox_get_content(raw()); }
		inline LvObjPtr getBtns() { return (LvObjPtr)lv_msgbox_get_btns(raw()); }
		inline uint16_t getActiveBtn() { return lv_msgbox_get_active_btn(raw()); }
		inline const char *getActiveBtnText() { return lv_msgbox_get_active_btn_text(raw()); }
		inline TSelf &close() { return lv_msgbox_close(raw()), _self(); }
		inline TSelf &closeAsync() { return lv_msgbox_close_async(raw()), _self(); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_msgbox_t>
	{
		using TWrapped = LvTMsgbox<lv_msgbox_t>;
		static constexpr auto &obj_class = lv_msgbox_class;
		static constexpr auto create = lv_msgbox_create;
	};

	using LvMsgbox = LvTMsgbox<lv_msgbox_t &&>;
	using LvMsgboxPtr = LvRawPtr<lv_msgbox_t>;

} /* namespace lvglpp */

#endif /* LVMSGBOX_H_ */
