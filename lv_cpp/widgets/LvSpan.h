
#ifndef LVSPAN_H_
#define LVSPAN_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTSpan : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setText(const char *text) { return lv_span_set_text(raw(), text), _self(); }
		inline TSelf &setTextStatic(const char *text) { return lv_span_set_text_static(raw(), text), _self(); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_span_t>
	{
		using TWrapped = LvTSpan<lv_span_t>;
		static constexpr auto &obj_class = lv_span_class;
		static constexpr auto create = lv_span_create;
	};

	using LvSpan = LvTSpan<lv_span_t &&>;
	using LvSpanPtr = LvRawPtr<lv_span_t>;

} /* namespace lvglpp */

#endif /* LVSPAN_H_ */
