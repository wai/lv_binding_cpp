
#ifndef LVIMG_H_
#define LVIMG_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTImg : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setSrc(const void *src) { return lv_img_set_src(raw(), src), _self(); }
		inline TSelf &setOffsetX(lv_coord_t x) { return lv_img_set_offset_x(raw(), x), _self(); }
		inline TSelf &setOffsetY(lv_coord_t y) { return lv_img_set_offset_y(raw(), y), _self(); }
		inline TSelf &setAngle(int16_t angle) { return lv_img_set_angle(raw(), angle), _self(); }
		inline TSelf &setPivot(lv_coord_t x, lv_coord_t y) { return lv_img_set_pivot(raw(), x, y), _self(); }
		inline TSelf &setZoom(uint16_t zoom) { return lv_img_set_zoom(raw(), zoom), _self(); }
		inline TSelf &setAntialias(bool antialias) { return lv_img_set_antialias(raw(), antialias), _self(); }
		inline TSelf &setSizeMode(lv_img_size_mode_t mode) { return lv_img_set_size_mode(raw(), mode), _self(); }
		inline const void *getSrc() { return lv_img_get_src(raw()); }
		inline lv_coord_t getOffsetX() { return lv_img_get_offset_x(raw()); }
		inline lv_coord_t getOffsetY() { return lv_img_get_offset_y(raw()); }
		inline uint16_t getAngle() { return lv_img_get_angle(raw()); }
		inline TSelf &getPivot(lv_point_t *pivot) { return lv_img_get_pivot(raw(), pivot), _self(); }
		inline uint16_t getZoom() { return lv_img_get_zoom(raw()); }
		inline bool getAntialias() { return lv_img_get_antialias(raw()); }
		inline lv_img_size_mode_t getSizeMode() { return lv_img_get_size_mode(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_img_t>
	{
		using TWrapped = LvTImg<lv_img_t>;
		static constexpr auto &obj_class = lv_img_class;
		static constexpr auto create = lv_img_create;
	};

	using LvImg = LvTImg<lv_img_t &&>;
	using LvImgPtr = LvRawPtr<lv_img_t>;

} /* namespace lvglpp */

#endif /* LVIMG_H_ */
