
#ifndef LVTABVIEW_H_
#define LVTABVIEW_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTTabview : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline LvObjPtr addTab(const char *name) { return (LvObjPtr)lv_tabview_add_tab(raw(), name); }
		inline LvObjPtr getContent() { return (LvObjPtr)lv_tabview_get_content(raw()); }
		inline LvObjPtr getTabBtns() { return (LvObjPtr)lv_tabview_get_tab_btns(raw()); }
		inline TSelf &setAct(uint32_t id, lv_anim_enable_t anim_en) { return lv_tabview_set_act(raw(), id, anim_en), _self(); }
		inline uint16_t getTabAct() { return lv_tabview_get_tab_act(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_tabview_t>
	{
		using TWrapped = LvTTabview<lv_tabview_t>;
		static constexpr auto &obj_class = lv_tabview_class;
		static constexpr auto create = lv_tabview_create;
	};

	using LvTabview = LvTTabview<lv_tabview_t &&>;
	using LvTabviewPtr = LvRawPtr<lv_tabview_t>;

} /* namespace lvglpp */

#endif /* LVTABVIEW_H_ */
