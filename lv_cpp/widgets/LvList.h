
#ifndef LVLIST_H_
#define LVLIST_H_

#include "../core/LvObj.h"
#include "./LvBtn.h"
#include "./LvLabel.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTList : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline LvLabelPtr addText(const char *txt) { return (LvLabelPtr)lv_list_add_text(raw(), txt); }
		inline LvBtnPtr addBtn(const char *icon, const char *txt) { return (LvBtnPtr)lv_list_add_btn(raw(), icon, txt); }
		inline const char *getBtnText(LvBtnPtr btn) { return lv_list_get_btn_text(raw(), btn); }
		//+gen
	};

	struct lv_list_t
	{
		lv_obj_t obj;
	};

	template <>
	struct LvProtypeTraits<lv_list_t>
	{
		using TWrapped = LvTList<lv_list_t>;
		static constexpr auto &obj_class = lv_list_class;
		static constexpr auto create = lv_list_create;
	};

	using LvList = LvTList<lv_list_t &&>;
	using LvListPtr = LvRawPtr<lv_list_t>;

} /* namespace lvglpp */

#endif /* LVLIST_H_ */
