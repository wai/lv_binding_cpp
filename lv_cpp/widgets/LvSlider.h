
#ifndef LVSLIDER_H_
#define LVSLIDER_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTSlider : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setValue(int32_t value, lv_anim_enable_t anim) { return lv_slider_set_value(raw(), value, anim), _self(); }
		inline TSelf &setLeftValue(int32_t value, lv_anim_enable_t anim) { return lv_slider_set_left_value(raw(), value, anim), _self(); }
		inline TSelf &setRange(int32_t min, int32_t max) { return lv_slider_set_range(raw(), min, max), _self(); }
		inline TSelf &setMode(lv_slider_mode_t mode) { return lv_slider_set_mode(raw(), mode), _self(); }
		inline int32_t getValue() const { return lv_slider_get_value(raw()); }
		inline int32_t getLeftValue() const { return lv_slider_get_left_value(raw()); }
		inline int32_t getMinValue() const { return lv_slider_get_min_value(raw()); }
		inline int32_t getMaxValue() const { return lv_slider_get_max_value(raw()); }
		inline bool isDragged() const { return lv_slider_is_dragged(raw()); }
		inline lv_slider_mode_t getMode() { return lv_slider_get_mode(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_slider_t>
	{
		using TWrapped = LvTSlider<lv_slider_t>;
		static constexpr auto &obj_class = lv_slider_class;
		static constexpr auto create = lv_slider_create;
	};

	using LvSlider = LvTSlider<lv_slider_t &&>;
	using LvSliderPtr = LvRawPtr<lv_slider_t>;

} /* namespace lvglpp */

#endif /* LVSLIDER_H_ */
