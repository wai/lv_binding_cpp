
#ifndef LVDROPDOWN_H_
#define LVDROPDOWN_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTDropdown : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setText(const char *txt) { return lv_dropdown_set_text(raw(), txt), _self(); }
		inline TSelf &setOptions(const char *options) { return lv_dropdown_set_options(raw(), options), _self(); }
		inline TSelf &setOptionsStatic(const char *options) { return lv_dropdown_set_options_static(raw(), options), _self(); }
		inline TSelf &addOption(const char *option, uint32_t pos) { return lv_dropdown_add_option(raw(), option, pos), _self(); }
		inline TSelf &clearOptions() { return lv_dropdown_clear_options(raw()), _self(); }
		inline TSelf &setSelected(uint16_t sel_opt) { return lv_dropdown_set_selected(raw(), sel_opt), _self(); }
		inline TSelf &setDir(lv_dir_t dir) { return lv_dropdown_set_dir(raw(), dir), _self(); }
		inline TSelf &setSymbol(const void *symbol) { return lv_dropdown_set_symbol(raw(), symbol), _self(); }
		inline TSelf &setSelectedHighlight(bool en) { return lv_dropdown_set_selected_highlight(raw(), en), _self(); }
		inline LvObjPtr getList() { return (LvObjPtr)lv_dropdown_get_list(raw()); }
		inline const char *getText() { return lv_dropdown_get_text(raw()); }
		inline const char *getOptions() const { return lv_dropdown_get_options(raw()); }
		inline uint16_t getSelected() const { return lv_dropdown_get_selected(raw()); }
		inline uint16_t getOptionCnt() const { return lv_dropdown_get_option_cnt(raw()); }
		inline const TSelf &getSelectedStr(char *buf, uint32_t buf_size) const { return lv_dropdown_get_selected_str(raw(), buf, buf_size), _self(); }
		inline const char *getSymbol() { return lv_dropdown_get_symbol(raw()); }
		inline bool getSelectedHighlight() { return lv_dropdown_get_selected_highlight(raw()); }
		inline lv_dir_t getDir() const { return lv_dropdown_get_dir(raw()); }
		inline TSelf &open() { return lv_dropdown_open(raw()), _self(); }
		inline TSelf &close() { return lv_dropdown_close(raw()), _self(); }
		inline bool isOpen() { return lv_dropdown_is_open(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_dropdown_t>
	{
		using TWrapped = LvTDropdown<lv_dropdown_t>;
		static constexpr auto &obj_class = lv_dropdown_class;
		static constexpr auto create = lv_dropdown_create;
	};

	using LvDropdown = LvTDropdown<lv_dropdown_t &&>;
	using LvDropdownPtr = LvRawPtr<lv_dropdown_t>;

} /* namespace lvglpp */

#endif /* LVDROPDOWN_H_ */
