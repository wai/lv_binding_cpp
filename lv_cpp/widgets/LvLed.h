
#ifndef LVLED_H_
#define LVLED_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTLed : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setColor(lv_color_t color) { return lv_led_set_color(raw(), color), _self(); }
		inline TSelf &setBrightness(uint8_t bright) { return lv_led_set_brightness(raw(), bright), _self(); }
		inline TSelf &on() { return lv_led_on(raw()), _self(); }
		inline TSelf &off() { return lv_led_off(raw()), _self(); }
		inline TSelf &toggle() { return lv_led_toggle(raw()), _self(); }
		inline uint8_t getBrightness() const { return lv_led_get_brightness(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_led_t>
	{
		using TWrapped = LvTLed<lv_led_t>;
		static constexpr auto &obj_class = lv_led_class;
		static constexpr auto create = lv_led_create;
	};

	using LvLed = LvTLed<lv_led_t &&>;
	using LvLedPtr = LvRawPtr<lv_led_t>;

} /* namespace lvglpp */

#endif /* LVLED_H_ */
