
#ifndef LVKEYBOARD_H_
#define LVKEYBOARD_H_

#include "./LvBtnmatrix.h"
#include "./LvTextarea.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTKeyboard : public LvTBtnmatrix<PROTYPE>
	{
	protected:
		using typename LvTBtnmatrix<PROTYPE>::TSelf;
		using LvTBtnmatrix<PROTYPE>::_self;

	public:
		using LvTBtnmatrix<PROTYPE>::raw;
		using LvTBtnmatrix<PROTYPE>::LvTBtnmatrix;
		//+gen
		inline TSelf &setTextarea(LvTextareaPtr ta) { return lv_keyboard_set_textarea(raw(), ta), _self(); }
		inline TSelf &setMode(lv_keyboard_mode_t mode) { return lv_keyboard_set_mode(raw(), mode), _self(); }
		inline TSelf &setPopovers(bool en) { return lv_keyboard_set_popovers(raw(), en), _self(); }
		inline TSelf &setMap(lv_keyboard_mode_t mode, const char *map[], const lv_btnmatrix_ctrl_t ctrl_map[]) { return lv_keyboard_set_map(raw(), mode, map, ctrl_map), _self(); }
		inline LvObjPtr getTextarea() const { return (LvObjPtr)lv_keyboard_get_textarea(raw()); }
		inline lv_keyboard_mode_t getMode() const { return lv_keyboard_get_mode(raw()); }
		inline const char **getMapArray() const { return lv_keyboard_get_map_array(raw()); }
		inline uint16_t getSelectedBtn() const { return lv_keyboard_get_selected_btn(raw()); }
		inline const char *getBtnText(uint16_t btn_id) const { return lv_keyboard_get_btn_text(raw(), btn_id); }
		inline TSelf &defEventCb() { return lv_keyboard_def_event_cb(raw()), _self(); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_keyboard_t>
	{
		using TWrapped = LvTKeyboard<lv_keyboard_t>;
		static constexpr auto &obj_class = lv_keyboard_class;
		static constexpr auto create = lv_keyboard_create;
		static inline lv_obj_t &get(lv_keyboard_t &keyboard) noexcept { return keyboard.btnm.obj; };
	};

	using LvKeyboard = LvTKeyboard<lv_keyboard_t &&>;
	using LvKeyboardPtr = LvRawPtr<lv_keyboard_t>;

} /* namespace lvglpp */

#endif /* LVKEYBOARD_H_ */
