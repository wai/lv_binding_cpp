
#ifndef LVLABEL_H_
#define LVLABEL_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTLabel : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setText(const char *text) { return lv_label_set_text(raw(), text), _self(); }
		inline TSelf &setTextFmt(const char *fmt) { return lv_label_set_text_fmt(raw(), fmt), _self(); }
		inline TSelf &setTextStatic(const char *text) { return lv_label_set_text_static(raw(), text), _self(); }
		inline TSelf &setLongMode(lv_label_long_mode_t long_mode) { return lv_label_set_long_mode(raw(), long_mode), _self(); }
		inline TSelf &setRecolor(bool en) { return lv_label_set_recolor(raw(), en), _self(); }
		inline TSelf &setTextSelStart(uint32_t index) { return lv_label_set_text_sel_start(raw(), index), _self(); }
		inline TSelf &setTextSelEnd(uint32_t index) { return lv_label_set_text_sel_end(raw(), index), _self(); }
		inline char *getText() const { return lv_label_get_text(raw()); }
		inline lv_label_long_mode_t getLongMode() const { return lv_label_get_long_mode(raw()); }
		inline bool getRecolor() const { return lv_label_get_recolor(raw()); }
		inline const TSelf &getLetterPos(uint32_t char_id, lv_point_t *pos) const { return lv_label_get_letter_pos(raw(), char_id, pos), _self(); }
		inline uint32_t getLetterOn(lv_point_t *pos_in) const { return lv_label_get_letter_on(raw(), pos_in); }
		inline bool isCharUnderPos(lv_point_t *pos) const { return lv_label_is_char_under_pos(raw(), pos); }
		inline uint32_t getTextSelectionStart() const { return lv_label_get_text_selection_start(raw()); }
		inline uint32_t getTextSelectionEnd() const { return lv_label_get_text_selection_end(raw()); }
		inline TSelf &insText(uint32_t pos, const char *txt) { return lv_label_ins_text(raw(), pos, txt), _self(); }
		inline TSelf &cutText(uint32_t pos, uint32_t cnt) { return lv_label_cut_text(raw(), pos, cnt), _self(); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_label_t>
	{
		using TWrapped = LvTLabel<lv_label_t>;
		static constexpr auto &obj_class = lv_label_class;
		static constexpr auto create = lv_label_create;
	};

	using LvLabel = LvTLabel<lv_label_t &&>;
	using LvLabelPtr = LvRawPtr<lv_label_t>;

} /* namespace lvglpp */

#endif /* LVLABEL_H_ */
