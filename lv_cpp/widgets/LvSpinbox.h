
#ifndef LVSPINBOX_H_
#define LVSPINBOX_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTSpinbox : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setValue(int32_t i) { return lv_spinbox_set_value(raw(), i), _self(); }
		inline TSelf &setRollover(bool b) { return lv_spinbox_set_rollover(raw(), b), _self(); }
		inline TSelf &setDigitFormat(uint8_t digit_count, uint8_t separator_position) { return lv_spinbox_set_digit_format(raw(), digit_count, separator_position), _self(); }
		inline TSelf &setStep(uint32_t step) { return lv_spinbox_set_step(raw(), step), _self(); }
		inline TSelf &setRange(int32_t range_min, int32_t range_max) { return lv_spinbox_set_range(raw(), range_min, range_max), _self(); }
		inline TSelf &setPos(uint8_t pos) { return lv_spinbox_set_pos(raw(), pos), _self(); }
		inline TSelf &setDigitStepDirection(lv_dir_t direction) { return lv_spinbox_set_digit_step_direction(raw(), direction), _self(); }
		inline bool getRollover() { return lv_spinbox_get_rollover(raw()); }
		inline int32_t getValue() { return lv_spinbox_get_value(raw()); }
		inline int32_t getStep() { return lv_spinbox_get_step(raw()); }
		inline TSelf &stepNext() { return lv_spinbox_step_next(raw()), _self(); }
		inline TSelf &stepPrev() { return lv_spinbox_step_prev(raw()), _self(); }
		inline TSelf &increment() { return lv_spinbox_increment(raw()), _self(); }
		inline TSelf &decrement() { return lv_spinbox_decrement(raw()), _self(); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_spinbox_t>
	{
		using TWrapped = LvTSpinbox<lv_spinbox_t>;
		static constexpr auto &obj_class = lv_spinbox_class;
		static constexpr auto create = lv_spinbox_create;
	};

	using LvSpinbox = LvTSpinbox<lv_spinbox_t &&>;
	using LvSpinboxPtr = LvRawPtr<lv_spinbox_t>;

} /* namespace lvglpp */

#endif /* LVSPINBOX_H_ */
