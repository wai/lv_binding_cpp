
#ifndef LVARC_H_
#define LVARC_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTArc : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setStartAngle(uint16_t start) { return lv_arc_set_start_angle(raw(), start), _self(); }
		inline TSelf &setEndAngle(uint16_t end) { return lv_arc_set_end_angle(raw(), end), _self(); }
		inline TSelf &setAngles(uint16_t start, uint16_t end) { return lv_arc_set_angles(raw(), start, end), _self(); }
		inline TSelf &setBgStartAngle(uint16_t start) { return lv_arc_set_bg_start_angle(raw(), start), _self(); }
		inline TSelf &setBgEndAngle(uint16_t end) { return lv_arc_set_bg_end_angle(raw(), end), _self(); }
		inline TSelf &setBgAngles(uint16_t start, uint16_t end) { return lv_arc_set_bg_angles(raw(), start, end), _self(); }
		inline TSelf &setRotation(uint16_t rotation) { return lv_arc_set_rotation(raw(), rotation), _self(); }
		inline TSelf &setMode(lv_arc_mode_t type) { return lv_arc_set_mode(raw(), type), _self(); }
		inline TSelf &setValue(int16_t value) { return lv_arc_set_value(raw(), value), _self(); }
		inline TSelf &setRange(int16_t min, int16_t max) { return lv_arc_set_range(raw(), min, max), _self(); }
		inline TSelf &setChangeRate(uint16_t rate) { return lv_arc_set_change_rate(raw(), rate), _self(); }
		inline uint16_t getAngleStart() { return lv_arc_get_angle_start(raw()); }
		inline uint16_t getAngleEnd() { return lv_arc_get_angle_end(raw()); }
		inline uint16_t getBgAngleStart() { return lv_arc_get_bg_angle_start(raw()); }
		inline uint16_t getBgAngleEnd() { return lv_arc_get_bg_angle_end(raw()); }
		inline int16_t getValue() const { return lv_arc_get_value(raw()); }
		inline int16_t getMinValue() const { return lv_arc_get_min_value(raw()); }
		inline int16_t getMaxValue() const { return lv_arc_get_max_value(raw()); }
		inline lv_arc_mode_t getMode() const { return lv_arc_get_mode(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_arc_t>
	{
		using TWrapped = LvTArc<lv_arc_t>;
		static constexpr auto &obj_class = lv_arc_class;
		static constexpr auto create = lv_arc_create;
	};

	using LvArc = LvTArc<lv_arc_t &&>;
	using LvArcPtr = LvRawPtr<lv_arc_t>;

} /* namespace lvglpp */

#endif /* LVARC_H_ */
