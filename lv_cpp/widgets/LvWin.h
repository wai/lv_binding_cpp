
#ifndef LVWIN_H_
#define LVWIN_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTWin : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline LvObjPtr addTitle(const char *txt) { return (LvObjPtr)lv_win_add_title(raw(), txt); }
		inline LvObjPtr addBtn(const void *icon, lv_coord_t btn_w) { return (LvObjPtr)lv_win_add_btn(raw(), icon, btn_w); }
		inline LvObjPtr getHeader() { return (LvObjPtr)lv_win_get_header(raw()); }
		inline LvObjPtr getContent() { return (LvObjPtr)lv_win_get_content(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_win_t>
	{
		using TWrapped = LvTWin<lv_win_t>;
		static constexpr auto &obj_class = lv_win_class;
		static constexpr auto create = lv_win_create;
	};

	using LvWin = LvTWin<lv_win_t &&>;
	using LvWinPtr = LvRawPtr<lv_win_t>;

} /* namespace lvglpp */

#endif /* LVWIN_H_ */
