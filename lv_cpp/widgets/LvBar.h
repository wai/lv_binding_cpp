
#ifndef LVBAR_H_
#define LVBAR_H_

#include "../core/LvObj.h"

namespace lvglpp
{
	template <typename PROTYPE>
	class LvTBar : public LvTObj<PROTYPE>
	{
	protected:
		using typename LvTObj<PROTYPE>::TSelf;
		using LvTObj<PROTYPE>::_self;

	public:
		using LvTObj<PROTYPE>::raw;
		using LvTObj<PROTYPE>::LvTObj;
		//+gen
		inline TSelf &setValue(int32_t value, lv_anim_enable_t anim) { return lv_bar_set_value(raw(), value, anim), _self(); }
		inline TSelf &setStartValue(int32_t start_value, lv_anim_enable_t anim) { return lv_bar_set_start_value(raw(), start_value, anim), _self(); }
		inline TSelf &setRange(int32_t min, int32_t max) { return lv_bar_set_range(raw(), min, max), _self(); }
		inline TSelf &setMode(lv_bar_mode_t mode) { return lv_bar_set_mode(raw(), mode), _self(); }
		inline int32_t getValue() const { return lv_bar_get_value(raw()); }
		inline int32_t getStartValue() const { return lv_bar_get_start_value(raw()); }
		inline int32_t getMinValue() const { return lv_bar_get_min_value(raw()); }
		inline int32_t getMaxValue() const { return lv_bar_get_max_value(raw()); }
		inline lv_bar_mode_t getMode() { return lv_bar_get_mode(raw()); }
		//+gen
	};

	template <>
	struct LvProtypeTraits<lv_bar_t>
	{
		using TWrapped = LvTBar<lv_bar_t>;
		static constexpr auto &obj_class = lv_bar_class;
		static constexpr auto create = lv_bar_create;
	};

	using LvBar = LvTBar<lv_bar_t &&>;
	using LvBarPtr = LvRawPtr<lv_bar_t>;

} /* namespace lvglpp */

#endif /* LVBAR_H_ */
